const immutableCollection = require("./");

let items = [{
	id: 1,
	color: "blue"
}, {
	id: 2,
	color: "red"
}, {
	id: 3,
	color: "red"
}, {
	id: 4,
	color: "red"
}, {
	id: 5,
	color: "blue"
}, {
	id: 6,
	color: "blue"
}, {
	id: 7,
	color: "red"
}];

let collection = immutableCollection(items);

let modified = collection.modify((collection) => {
	return collection
		.subset((blueItems) => {
			return blueItems
				.select((item) => item.color === "blue")
				.remove();
		})
		.subset((oddItems) => {
			return oddItems
				.select((item) => item.id % 2 === 1)
				.map((item) => ({ ... item, color: "green" }));
		})
		.map((item) => ({ ... item, id: item.id * 2 }));
});

console.log(modified);